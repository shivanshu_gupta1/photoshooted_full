import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

// Pages
import Home from './pages/Home';
import Funktionen from './pages/Funktionen';
import Impressum from './pages/Impressum';
import Kontakt from './pages/Kontakt';
import Preise from './pages/Preise';

function App() {
  return (
    <Router>
      <Route path="/" exact component={Home} />
      <Route path="/funktionen" exact component={Funktionen} />
      <Route path="/impressum-datenschutzerklaerung" exact component={Impressum} />
      <Route path="/kontakt" exact component={Kontakt} />
      <Route path="/preise" exact component={Preise} />
      <Route path="/cookie-richtlinie-eu" exact component={Impressum} />
    </Router>
  );
}

export default App;
